# English for Devs

This is a handbook for software developers who speak English as a second language.

Contributions are welcome.

-----

# Phrases
In this section we collect a list of ready to use phrases for your day to day job as a software developer. 

## Asking for help 
  - Can someone help me with this ticket? 
  - 

## Meetings
  - How's it going? 
  - 

## Grammar hacks

### Use "they"

Instead of using he/she you can make your sentences simpler by using "they"

Example:

OK:

If a client finds a bug, he/she can report it to me. 

Better:

If a client finds a bug, **they** can report it to me. 


------


# Resources 

In this section you'll find links to various resources that you can use to improve your English as a software developer. 

## podcasts

  * The changelog is a nice podcast with transcriptions available. Listening to the podcasts and taking a look at the transcripts when needed can improve your English a lot. 

[The Changelog](https://changelog.com)



